#!/bin/bash

# Adaptado do original encontrado em 

# http://jitclouds.lsd.ufcg.edu.br/downloads/preconfigured/install_jitcloud.sh



pause()

{

    key=""

    echo -n Hit any key to continue....

    stty -icanon

    key=`dd count=1 2>/dev/null`

    stty icanon

}



# just in case

sh cloudmiddleware/bin/jitcloud stop > /dev/null 2>&1

sh dcmiddleware/bin/jitdc stop > /dev/null 2>&1



echo -n "Do you want to download JiT Cloud components? (yes/no) "

read REPLY

if [ "$REPLY" = "yes" ]

then



echo "Going to download all the JiT Cloud preconfigured components."

pause


wget http://jitclouds.lsd.ufcg.edu.br/downloads/cloudclient-1.0.zip

wget http://jitclouds.lsd.ufcg.edu.br/downloads/cloudmiddleware-1.0.zip

wget http://jitclouds.lsd.ufcg.edu.br/downloads/dcmiddleware-1.0.zip


mv cloudclient-1.0.zip  cloudclient.zip

mv cloudmiddleware-1.0.zip  cloudmiddleware.zip

mv dcmiddleware-1.0.zip dcmiddleware.zip


if [ ! -e cloudclient.zip ]

then

    echo 'Could not download cloudclient. Please check your Internet conection.'

    exit 1

fi



if [ ! -e cloudmiddleware.zip ]

then

    echo 'Could not download cloudmiddleware. Please check your Internet conection.'

    exit 2

fi



if [ ! -e dcmiddleware.zip ]

then

    echo 'Could not download dcmiddleware. Please check your Internet conection.'

    exit 3

fi



fi



echo -n "Do you want to unzip all packages and maybe overwrite all JiT Cloud components directories? (yes/no) "

read REPLY

if [ "$REPLY" = "yes" ]

then

    rm -rf cloudmiddleware/logs/* 

    rm -rf dcmiddleware/logs/*

    rm -rf cloudmiddleware/db



    unzip cloudclient.zip

    unzip cloudmiddleware.zip

    unzip dcmiddleware.zip

fi



echo "Please enter the following properties..."

#pause



echo -n "Enter eucalyptus front-end IP: "

read EUCALYPTUS_FRONTEND

echo -n "Enter eucalyptus admin Access Key: "

read ACCESSKEY

echo -n "Enter eucalyptus admin Secret Key: "

read SECRET_ACCESSKEY

echo -n "Enter eucalyptus default vm image id: "

read VM_IMAGE_ID





echo "Please check the properties..."

echo " "



echo "Eucalyptus front-end: " $EUCALYPTUS_FRONTEND

echo "Eucalyptus accessKey: " $ACCESSKEY

echo "Eucalyptus secretAccessKey: " $SECRET_ACCESSKEY

echo "Eucalyptus default vm image id: " $VM_IMAGE_ID



echo " "

echo -n "Are they correct? (yes/no) "

read REPLY

if [ "$REPLY" != "yes" ]

then

    echo 'Run this script again and inform the proper answers.'

    exit 0

else

    echo "Going to change properties in cloudmiddleware/etc/conf.properties..."

    sed -i "s/<jit_dc_1_access_key>/$ACCESSKEY/g" cloudmiddleware/etc/conf.properties

    sed -i "s/<jit_dc_1_secret_access_key>/$SECRET_ACCESSKEY/g" cloudmiddleware/etc/conf.properties

    sed -i "s/<jit_dc_1_default_vm_image_id>/$VM_IMAGE_ID/g" cloudmiddleware/etc/conf.properties

    echo "Done!"

    echo "Going to change properties in dcmiddleware/etc/conf.properties..."

    sed -i "s/<eucalyptus-frontend>/$EUCALYPTUS_FRONTEND/g" dcmiddleware/etc/conf.properties

    sed -i "s/<eucalyptus-access-key>/$ACCESSKEY/g" dcmiddleware/etc/conf.properties

    sed -i "s/<eucalyptus-secret-access-key>/$SECRET_ACCESSKEY/g" dcmiddleware/etc/conf.properties

    sed -i "s/<default-vm-image-id>/$VM_IMAGE_ID/g" dcmiddleware/etc/conf.properties

    echo "Done!"

    echo "Going to register the JiT Data Center..."

fi



echo "Starting cloud and dc middlewares... "



sh cloudmiddleware/bin/jitcloud start



sh dcmiddleware/bin/jitdc start



sleep 20



echo "Inspect the logs. All *dependencies.log must be empty!"



ls -lh cloudmiddleware/logs/ dcmiddleware/logs/



echo " "

pause



echo "Going to access the current deployed JiT Cloud:"



export JITCLIENT_HOME=`pwd`/cloudclient/

echo "Describe clients: "

sh $JITCLIENT_HOME/bin/jit-describe-clients

echo "Describe JiT DCs: "

sh $JITCLIENT_HOME/bin/jit-describe-dcs

echo "Register local JiT DC: "

sh $JITCLIENT_HOME/bin/jit-register-dc -n jit_dc_1 -e http://localhost:65082/services/JiTDCUserAPIService -a http://localhost:65082/services/JiTDCAdminAPIService

echo "Describe JiT DCs again: "

sh $JITCLIENT_HOME/bin/jit-describe-dcs

echo "Describe registered vm images: "

sh $JITCLIENT_HOME/bin/jit-describe-images

echo "Describe running instances: "

sh $JITCLIENT_HOME/bin/jit-describe-instances

echo "Describe JiT DC potential: "

sh $JITCLIENT_HOME/bin/jit-describe-potential -n 1 -t m1.small -i jmi-default





echo " "

echo "In order to run the cloud client please do the following before running any JiT Cloud Client command: "

echo "export JITCLIENT_HOME="$JITCLIENT_HOME



exit 0

